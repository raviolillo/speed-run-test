package com.twentyonebuttons.apidatasource

import com.twentyonebuttons.apidatasource.client.ApiClient
import com.twentyonebuttons.apidatasource.mappers.GamesMapper
import com.twentyonebuttons.apidatasource.mappers.PlayerDetailsMapper
import com.twentyonebuttons.apidatasource.mappers.SpeedRunsMapper
import com.twentyonebuttons.apidatasource.models.GamesApiModel
import com.twentyonebuttons.apidatasource.models.PlayerDetailsApiModel
import com.twentyonebuttons.apidatasource.models.SpeedRunsApiModel
import com.twentyonebuttons.apidatasource.service.ApiService
import com.twentyonebuttons.apidatasource.utils.any
import com.twentyonebuttons.domain.models.GamesModel
import com.twentyonebuttons.domain.models.PlayerDetailsModel
import com.twentyonebuttons.domain.models.SpeedRunsModel
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class ApiDataSourceImplTest {

    @Mock
    private lateinit var apiService: ApiService
    @Mock
    private lateinit var apiClient: ApiClient
    @Mock
    private lateinit var gamesMapper: GamesMapper
    @Mock
    private lateinit var gamesApiModel: GamesApiModel
    @Mock
    private lateinit var gamesModel: GamesModel
    @Mock
    private lateinit var playerDetailsMapper: PlayerDetailsMapper
    @Mock
    private lateinit var playerDetailsApiModel: PlayerDetailsApiModel
    @Mock
    private lateinit var playerDetailsModel: PlayerDetailsModel
    @Mock
    private lateinit var speedRunsMapper: SpeedRunsMapper
    @Mock
    private lateinit var speedRunsApiModel: SpeedRunsApiModel
    @Mock
    private lateinit var speedRunsModel: SpeedRunsModel

    private lateinit var apiDataSourceImpl: ApiDataSourceImpl

    /**
     * Setup Mockito and initial mocks here
     */

    private fun mockApiService() {
        `when`(apiClient.service)
            .thenReturn(apiService)
    }

    private fun mockGamesMapper(gamesModel: GamesModel) {
        `when`(gamesMapper.map(any()))
            .thenReturn(gamesModel)
    }

    private fun mockSpeedRunsMapper(speedRunsModel: SpeedRunsModel) {
        `when`(speedRunsMapper.map(any()))
            .thenReturn(speedRunsModel)
    }

    private fun mockPlayerDetailsMapper(playerDetailsModel: PlayerDetailsModel) {
        `when`(playerDetailsMapper.map(any()))
            .thenReturn(playerDetailsModel)
    }

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        mockApiService()
        mockGamesMapper(gamesModel)
        mockSpeedRunsMapper(speedRunsModel)
        mockPlayerDetailsMapper(playerDetailsModel)
        apiDataSourceImpl = ApiDataSourceImpl(
            apiClient,
            gamesMapper,
            speedRunsMapper,
            playerDetailsMapper
        )
    }

    /** Get Games Tests **/

    private fun mockApiServiceGetGames(
        gamesApiModel: GamesApiModel,
        pageNumber: Int,
        gamesSize: Int
    ) {
        `when`(apiService.getGames(pageNumber, gamesSize))
            .thenReturn(Single.just(gamesApiModel))
    }

    @Test
    fun `Map Games Response Api Model to Games Response Pure Model when performing getGames`() {
        val pageNumber = 0
        val gamesSize = 20
        mockApiServiceGetGames(gamesApiModel, pageNumber, gamesSize)

        apiDataSourceImpl.getGames(pageNumber, gamesSize).blockingGet()

        verify(gamesMapper, only()).map(gamesApiModel)
    }

    @Test
    fun `Call Api Service to get games when performing getGames`() {
        val pageNumber = 0
        val gamesSize = 20
        mockApiServiceGetGames(gamesApiModel, pageNumber, gamesSize)

        apiDataSourceImpl.getGames(pageNumber, gamesSize).blockingGet()

        verify(apiService, only()).getGames(pageNumber, gamesSize)
    }

    @Test
    fun `Api Service should return Games mapped response when performing getGames`() {
        val pageNumber = 0
        val gamesSize = 20
        mockApiServiceGetGames(gamesApiModel, pageNumber, gamesSize)

        val mappedResponse = apiDataSourceImpl.getGames(pageNumber, gamesSize).blockingGet()

        assertEquals(gamesModel, mappedResponse)
    }

    /** Get Speed Runs Tests **/

    private fun mockApiServiceGetSpeedRuns(speedRunsApiModel: SpeedRunsApiModel) {
        `when`(apiService.getGameSpeedRuns(any()))
            .thenReturn(Single.just(speedRunsApiModel))
    }

    @Test
    fun `Map Speed Runs Response Api Model to Speed Runs Response Pure Model when performing getGameSpeedRuns`() {
        val gameId = "test1"
        mockApiServiceGetSpeedRuns(speedRunsApiModel)

        apiDataSourceImpl.getGameSpeedRuns(gameId).blockingGet()

        verify(speedRunsMapper, only()).map(speedRunsApiModel)
    }

    @Test
    fun `Call Api Service to get speed runs when performing getGameSpeedRuns`() {
        val gameId = "test2"
        mockApiServiceGetSpeedRuns(speedRunsApiModel)

        apiDataSourceImpl.getGameSpeedRuns(gameId).blockingGet()

        verify(apiService, only()).getGameSpeedRuns(gameId)
    }

    @Test
    fun `Api Service should return Speed Runs mapped response when performing getGameSpeedRuns`() {
        val gameId = "test3"
        mockApiServiceGetSpeedRuns(speedRunsApiModel)

        val mappedResponse = apiDataSourceImpl.getGameSpeedRuns(gameId).blockingGet()

        assertEquals(speedRunsModel, mappedResponse)
    }

    /** Get Player Details Tests **/

    private fun mockApiServiceGetPlayerDetails(playerDetailsApiModel: PlayerDetailsApiModel) {
        `when`(apiService.getPlayerDetails(any()))
            .thenReturn(Single.just(playerDetailsApiModel))
    }

    @Test
    fun `Map Player Details Response Api Model to Player Details Response Pure Model when performing getPlayerDetails`() {
        val playerId = "test1"
        mockApiServiceGetPlayerDetails(playerDetailsApiModel)

        apiDataSourceImpl.getPlayerDetails(playerId).blockingGet()

        verify(playerDetailsMapper, only()).map(playerDetailsApiModel)
    }

    @Test
    fun `Call Api Service to get player info when performing getPlayerDetails`() {
        val playerId = "test2"
        mockApiServiceGetPlayerDetails(playerDetailsApiModel)

        apiDataSourceImpl.getPlayerDetails(playerId).blockingGet()

        verify(apiService, only()).getPlayerDetails(playerId)
    }

    @Test
    fun `Api Service should return Player Details mapped response when performing getPlayerDetails`() {
        val playerId = "test3"
        mockApiServiceGetPlayerDetails(playerDetailsApiModel)

        val mappedResponse = apiDataSourceImpl.getPlayerDetails(playerId).blockingGet()

        assertEquals(playerDetailsModel, mappedResponse)
    }
}
