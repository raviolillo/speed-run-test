package com.twentyonebuttons.apidatasource.service

import com.twentyonebuttons.apidatasource.models.GamesApiModel
import com.twentyonebuttons.apidatasource.models.PlayerDetailsApiModel
import com.twentyonebuttons.apidatasource.models.SpeedRunsApiModel
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("$API_VERSION/games")
    fun getGames(@Query("offset") offset: Int, @Query("max") max: Int): Single<GamesApiModel>

    @GET("$API_VERSION/runs")
    fun getGameSpeedRuns(@Query("game") gameId: String): Single<SpeedRunsApiModel>

    @GET("$API_VERSION/users/{user}")
    fun getPlayerDetails(@Path("user") userId: String): Single<PlayerDetailsApiModel>

    companion object {
        private const val API_VERSION = "api/v1"
    }
}
