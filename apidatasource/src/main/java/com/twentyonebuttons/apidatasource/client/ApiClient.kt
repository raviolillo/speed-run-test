package com.twentyonebuttons.apidatasource.client

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.twentyonebuttons.apidatasource.service.ApiService
import io.reactivex.Scheduler
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient(isDebug: Boolean) {

    private val gson: Gson by lazy { GsonBuilder().create() }
    private val schedulerType: Scheduler by lazy { Schedulers.io() }

    private val client by lazy {
        OkHttpClient().newBuilder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = if (isDebug) HttpLoggingInterceptor.Level.BODY
                else HttpLoggingInterceptor.Level.NONE
            })
            .build()
    }

    @Suppress("DEPRECATION")
    val service: ApiService by lazy {
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(schedulerType))
            .build()
            .create(ApiService::class.java)
    }

    companion object {
        private const val BASE_URL = "http://www.speedrun.com/"
    }
}
