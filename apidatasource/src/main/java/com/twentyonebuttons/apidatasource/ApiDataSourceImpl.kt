package com.twentyonebuttons.apidatasource

import com.twentyonebuttons.apidatasource.client.ApiClient
import com.twentyonebuttons.apidatasource.mappers.GamesMapper
import com.twentyonebuttons.apidatasource.mappers.PlayerDetailsMapper
import com.twentyonebuttons.apidatasource.mappers.SpeedRunsMapper
import com.twentyonebuttons.data.datasources.ApiDataSource
import com.twentyonebuttons.domain.models.GamesModel
import com.twentyonebuttons.domain.models.PlayerDetailsModel
import com.twentyonebuttons.domain.models.SpeedRunsModel
import io.reactivex.Single
import javax.inject.Inject

class ApiDataSourceImpl @Inject constructor(
    apiClient: ApiClient,
    private val gamesMapper: GamesMapper,
    private val speedRunsMapper: SpeedRunsMapper,
    private val playerDetailsMapper: PlayerDetailsMapper
) : ApiDataSource {

    private val service by lazy { apiClient.service }

    override fun getGames(pageNumber: Int, max: Int): Single<GamesModel> =
        service.getGames(pageNumber * max, max).map(gamesMapper::map)

    override fun getGameSpeedRuns(gameId: String): Single<SpeedRunsModel> =
        service.getGameSpeedRuns(gameId).map(speedRunsMapper::map)

    override fun getPlayerDetails(userId: String): Single<PlayerDetailsModel> =
        service.getPlayerDetails(userId).map(playerDetailsMapper::map)
}
