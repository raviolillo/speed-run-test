package com.twentyonebuttons.apidatasource.models

import com.google.gson.annotations.SerializedName

data class SpeedRunsApiModel(
    @SerializedName("data")
    val data: List<SpeedRunApiModel>
)
