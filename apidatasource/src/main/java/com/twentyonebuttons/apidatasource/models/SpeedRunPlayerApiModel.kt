package com.twentyonebuttons.apidatasource.models

import com.google.gson.annotations.SerializedName

data class SpeedRunPlayerApiModel(
    @SerializedName("rel")
    val type: String,
    @SerializedName("id")
    val id: String?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("uri")
    val uri: String
)
