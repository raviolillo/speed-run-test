package com.twentyonebuttons.apidatasource.models

import com.google.gson.annotations.SerializedName

data class GameImageApiModel(
    @SerializedName("uri")
    val uri: String
)
