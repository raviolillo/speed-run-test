package com.twentyonebuttons.apidatasource.models

import com.google.gson.annotations.SerializedName

data class SpeedRunVideoUriApiModel(
    @SerializedName("uri")
    val uri: String
)
