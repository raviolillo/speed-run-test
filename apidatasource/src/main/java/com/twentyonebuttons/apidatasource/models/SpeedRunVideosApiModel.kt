package com.twentyonebuttons.apidatasource.models

import com.google.gson.annotations.SerializedName

data class SpeedRunVideosApiModel(
    @SerializedName("links")
    val links: List<SpeedRunVideoUriApiModel>
)
