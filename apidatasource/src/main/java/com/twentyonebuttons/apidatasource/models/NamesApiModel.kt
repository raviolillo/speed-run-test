package com.twentyonebuttons.apidatasource.models

import com.google.gson.annotations.SerializedName

data class NamesApiModel(
    @SerializedName("international")
    val international: String
)
