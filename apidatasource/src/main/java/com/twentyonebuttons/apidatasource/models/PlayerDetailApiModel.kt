package com.twentyonebuttons.apidatasource.models

import com.google.gson.annotations.SerializedName

data class PlayerDetailApiModel(
    @SerializedName("id")
    val id: String,
    @SerializedName("names")
    val names: NamesApiModel
)
