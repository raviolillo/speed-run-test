package com.twentyonebuttons.apidatasource.models

import com.google.gson.annotations.SerializedName

data class GameApiModel(
    @SerializedName("id")
    val id: String,
    @SerializedName("names")
    val names: NamesApiModel,
    @SerializedName("assets")
    val assets: GameAssetsApiModel
)
