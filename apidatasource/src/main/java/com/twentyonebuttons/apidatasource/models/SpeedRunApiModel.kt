package com.twentyonebuttons.apidatasource.models

import com.google.gson.annotations.SerializedName

data class SpeedRunApiModel(
    @SerializedName("id")
    val id: String,
    @SerializedName("videos")
    val videos: SpeedRunVideosApiModel?,
    @SerializedName("players")
    val players: List<SpeedRunPlayerApiModel>,
    @SerializedName("times")
    val times: SpeedRunTimesApiModel
)
