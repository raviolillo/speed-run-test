package com.twentyonebuttons.apidatasource.models

import com.google.gson.annotations.SerializedName

data class GameAssetsApiModel(
    @SerializedName("cover-small")
    val smallLogo: GameImageApiModel,
    @SerializedName("cover-large")
    val bigLogo: GameImageApiModel
)