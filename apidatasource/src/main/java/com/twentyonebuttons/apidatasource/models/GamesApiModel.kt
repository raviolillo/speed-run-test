package com.twentyonebuttons.apidatasource.models

import com.google.gson.annotations.SerializedName

data class GamesApiModel(
    @SerializedName("data")
    val data: List<GameApiModel>
)
