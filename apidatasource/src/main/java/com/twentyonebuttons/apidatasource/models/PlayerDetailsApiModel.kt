package com.twentyonebuttons.apidatasource.models

import com.google.gson.annotations.SerializedName

data class PlayerDetailsApiModel(
    @SerializedName("data")
    val data: PlayerDetailApiModel
)
