package com.twentyonebuttons.apidatasource.models

import com.google.gson.annotations.SerializedName

data class SpeedRunTimesApiModel(
    @SerializedName("realtime_t")
    val realTimeSec: Int
)
