package com.twentyonebuttons.apidatasource.mappers

import com.twentyonebuttons.apidatasource.models.GamesApiModel
import com.twentyonebuttons.domain.models.GamesModel
import com.twentyonebuttons.domain.utils.Mapper
import javax.inject.Inject

class GamesMapper @Inject constructor(
    private val gameListMapper: GameListMapper
) : Mapper<GamesApiModel, GamesModel> {

    override fun map(from: GamesApiModel): GamesModel =
        GamesModel(gameListMapper.map(from.data))
}
