package com.twentyonebuttons.apidatasource.mappers

import com.twentyonebuttons.apidatasource.models.PlayerDetailsApiModel
import com.twentyonebuttons.domain.models.PlayerDetailsModel
import com.twentyonebuttons.domain.utils.Mapper
import javax.inject.Inject

class PlayerDetailsMapper @Inject constructor(
    private val playerDetailMapper: PlayerDetailMapper
) : Mapper<PlayerDetailsApiModel, PlayerDetailsModel> {

    override fun map(from: PlayerDetailsApiModel): PlayerDetailsModel =
        PlayerDetailsModel(playerDetailMapper.map(from.data))
}
