package com.twentyonebuttons.apidatasource.mappers

import com.twentyonebuttons.apidatasource.models.SpeedRunsApiModel
import com.twentyonebuttons.domain.models.SpeedRunsModel
import com.twentyonebuttons.domain.utils.Mapper
import javax.inject.Inject

class SpeedRunsMapper @Inject constructor(
    private val speedRunListMapper: SpeedRunListMapper
) : Mapper<SpeedRunsApiModel, SpeedRunsModel> {

    override fun map(from: SpeedRunsApiModel): SpeedRunsModel =
        SpeedRunsModel(speedRunListMapper.map(from.data))
}
