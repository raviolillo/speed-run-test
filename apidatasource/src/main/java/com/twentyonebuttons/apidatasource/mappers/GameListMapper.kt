package com.twentyonebuttons.apidatasource.mappers

import com.twentyonebuttons.apidatasource.models.GameApiModel
import com.twentyonebuttons.domain.models.GameModel
import com.twentyonebuttons.domain.utils.ListMapper
import javax.inject.Inject

class GameListMapper @Inject constructor() :
    ListMapper<GameApiModel, GameModel> {

    override fun map(from: GameApiModel): GameModel =
        GameModel(
            from.id,
            from.names.international,
            from.assets.smallLogo.uri,
            from.assets.bigLogo.uri
        )
}
