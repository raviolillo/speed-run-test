package com.twentyonebuttons.apidatasource.mappers

import com.twentyonebuttons.apidatasource.models.SpeedRunApiModel
import com.twentyonebuttons.domain.models.SpeedRunModel
import com.twentyonebuttons.domain.utils.ListMapper
import javax.inject.Inject

class SpeedRunListMapper @Inject constructor(private val speedRunPlayerMapper: SpeedRunPlayerMapper) :
    ListMapper<SpeedRunApiModel, SpeedRunModel> {

    override fun map(from: SpeedRunApiModel): SpeedRunModel =
        SpeedRunModel(
            from.id,
            from.videos?.links?.get(0)?.uri ?: "",
            speedRunPlayerMapper.map(from.players[0]),
            from.times.realTimeSec
        )
}
