package com.twentyonebuttons.apidatasource.mappers

import com.twentyonebuttons.apidatasource.models.PlayerDetailApiModel
import com.twentyonebuttons.domain.models.PlayerDetailModel
import com.twentyonebuttons.domain.utils.Mapper
import javax.inject.Inject

class PlayerDetailMapper @Inject constructor() :
    Mapper<PlayerDetailApiModel, PlayerDetailModel> {

    override fun map(from: PlayerDetailApiModel): PlayerDetailModel =
        PlayerDetailModel(
            from.id,
            from.names.international
        )
}
