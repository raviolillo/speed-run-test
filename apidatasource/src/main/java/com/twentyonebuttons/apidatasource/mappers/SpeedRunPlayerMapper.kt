package com.twentyonebuttons.apidatasource.mappers

import com.twentyonebuttons.apidatasource.models.SpeedRunPlayerApiModel
import com.twentyonebuttons.domain.models.SpeedRunPlayerModel
import com.twentyonebuttons.domain.utils.Mapper
import javax.inject.Inject

class SpeedRunPlayerMapper @Inject constructor() :
    Mapper<SpeedRunPlayerApiModel, SpeedRunPlayerModel> {

    override fun map(from: SpeedRunPlayerApiModel): SpeedRunPlayerModel =
        SpeedRunPlayerModel(
            from.type,
            from.id,
            from.name,
            from.uri
        )
}
