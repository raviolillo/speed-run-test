# README #

Technical test application created to show the world records speedruns.

### Documentation ###

The default language to make this Android Application is Kotlin <https://developer.android.com/kotlin>

In order to reduce the size, minify, shrink and obfuscate the code as much as possible, this project will use Proguard <https://developer.android.com/studio/build/shrink-code>

In order to generate the documentation, this project uses Dokka <https://github.com/Kotlin/dokka>

```./gradlew dokka```

By default, our output format is **Javadoc**. However there are other output formats supported:

* Html: Minimalistic hypertext markup language format
* Markdown: Lightweight markup language with plain text formatting syntax

### Company ###

* **21 Buttons**