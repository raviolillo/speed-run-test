package com.twentyonebuttons.components

import android.app.Dialog
import android.app.ProgressDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.twentyonebuttons.components.utils.safeShow

class ProgressDialogFragment : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isCancelable = false
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        @Suppress("DEPRECATION")
        with(ProgressDialog(context)) {
            // Configure the progress dialog
            isIndeterminate = true
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            safeShow()
            setContentView(R.layout.cv_progress_dialog)
            // Return the modified dialog instance
            this
        }
}
