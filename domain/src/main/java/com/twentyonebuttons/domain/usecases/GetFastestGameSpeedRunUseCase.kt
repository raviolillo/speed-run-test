package com.twentyonebuttons.domain.usecases

import com.twentyonebuttons.domain.models.SpeedRunModel
import com.twentyonebuttons.domain.repositories.Repository
import io.reactivex.Single
import javax.inject.Inject

class GetFastestGameSpeedRunUseCase @Inject constructor(private val repository: Repository) {

    fun execute(gameId: String): Single<SpeedRunModel> = repository.getFastestGameSpeedRun(gameId)
}
