package com.twentyonebuttons.domain.usecases

import com.twentyonebuttons.domain.models.GamesModel
import com.twentyonebuttons.domain.repositories.Repository
import io.reactivex.Single
import javax.inject.Inject

class GetGamesUseCase @Inject constructor(private val repository: Repository) {

    fun execute(pageNumber: Int, max: Int = 20): Single<GamesModel> =
        repository.getGames(pageNumber, max)
}
