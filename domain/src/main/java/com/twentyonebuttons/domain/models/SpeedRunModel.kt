package com.twentyonebuttons.domain.models

data class SpeedRunModel(
    val id: String = "",
    val videoUri: String = "",
    val firstPlayer: SpeedRunPlayerModel = SpeedRunPlayerModel(),
    val timeSec: Int = 0
)
