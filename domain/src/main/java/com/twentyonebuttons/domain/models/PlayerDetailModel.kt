package com.twentyonebuttons.domain.models

data class PlayerDetailModel(
    val id: String,
    val name: String
)
