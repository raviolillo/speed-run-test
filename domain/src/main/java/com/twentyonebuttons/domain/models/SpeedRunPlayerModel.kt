package com.twentyonebuttons.domain.models

data class SpeedRunPlayerModel(
    val type: String = "",
    val id: String? = null,
    var name: String? = null,
    val uri: String = ""
)
