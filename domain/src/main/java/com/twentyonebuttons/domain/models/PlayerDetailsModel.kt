package com.twentyonebuttons.domain.models

data class PlayerDetailsModel(
    val data: PlayerDetailModel
)
