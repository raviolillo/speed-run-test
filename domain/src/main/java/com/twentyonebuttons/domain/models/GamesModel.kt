package com.twentyonebuttons.domain.models

data class GamesModel(
    val data: List<GameModel>
)
