package com.twentyonebuttons.domain.models

data class GameModel(
    val id: String,
    val name: String,
    val smallLogoUri: String,
    val bigLogoUri: String
)
