package com.twentyonebuttons.domain.models

data class SpeedRunsModel(
    val data: List<SpeedRunModel>
)
