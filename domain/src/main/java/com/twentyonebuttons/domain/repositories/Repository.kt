package com.twentyonebuttons.domain.repositories

import com.twentyonebuttons.domain.models.GamesModel
import com.twentyonebuttons.domain.models.PlayerDetailsModel
import com.twentyonebuttons.domain.models.SpeedRunModel
import io.reactivex.Single

interface Repository {

    fun getGames(pageNumber: Int, max: Int): Single<GamesModel>

    fun getFastestGameSpeedRun(gameId: String): Single<SpeedRunModel>

    fun getPlayerDetails(userId: String): Single<PlayerDetailsModel>
}
