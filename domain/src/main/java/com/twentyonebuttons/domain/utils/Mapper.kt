package com.twentyonebuttons.domain.utils

interface Mapper<in FROM, out TO> {

    fun map(from: FROM): TO
}
