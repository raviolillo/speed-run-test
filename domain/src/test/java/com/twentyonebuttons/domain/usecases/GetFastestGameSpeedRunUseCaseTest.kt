package com.twentyonebuttons.domain.usecases

import com.twentyonebuttons.domain.models.SpeedRunModel
import com.twentyonebuttons.domain.repositories.Repository
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class GetFastestGameSpeedRunUseCaseTest {

    @Mock
    private lateinit var repository: Repository
    @Mock
    private lateinit var speedRunModel: Single<SpeedRunModel>

    private lateinit var getFastestGameSpeedRunUseCase: GetFastestGameSpeedRunUseCase

    /**
     * Setup Mockito here
     */

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        getFastestGameSpeedRunUseCase = GetFastestGameSpeedRunUseCase(repository)
    }

    /**
     * Test use case
     */

    private fun setupRepository(gameId: String) {
        `when`(repository.getFastestGameSpeedRun(gameId))
            .thenReturn(speedRunModel)
    }

    @Test
    fun `Call the Repository to get fastest speed run from a game when executed`() {
        val gameId = "test_game"
        setupRepository(gameId)

        getFastestGameSpeedRunUseCase.execute(gameId)

        verify(repository, only()).getFastestGameSpeedRun(gameId)
    }

    @Test
    fun `Return the expected value after calling the Repository to get fastest speed run from a game when being executed`() {
        val gameId = "test_game"
        setupRepository(gameId)

        val response = getFastestGameSpeedRunUseCase.execute(gameId)

        assertEquals(speedRunModel, response)
    }
}
