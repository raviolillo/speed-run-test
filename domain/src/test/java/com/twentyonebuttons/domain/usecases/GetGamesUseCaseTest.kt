package com.twentyonebuttons.domain.usecases

import com.twentyonebuttons.domain.models.GamesModel
import com.twentyonebuttons.domain.repositories.Repository
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class GetGamesUseCaseTest {

    @Mock
    private lateinit var repository: Repository
    @Mock
    private lateinit var gamesModel: Single<GamesModel>

    private lateinit var getGamesUseCase: GetGamesUseCase

    /**
     * Setup Mockito here
     */

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        getGamesUseCase = GetGamesUseCase(repository)
    }

    /**
     * Test use case
     */

    private fun setupRepository(pageNumber: Int, gamesSize: Int) {
        `when`(repository.getGames(pageNumber, gamesSize))
            .thenReturn(gamesModel)
    }

    @Test
    fun `Call the Repository to get Games when executed`() {
        val pageNumber = 0
        val gamesSize = 20
        setupRepository(pageNumber, gamesSize)

        getGamesUseCase.execute(pageNumber, gamesSize)

        verify(repository, only()).getGames(pageNumber, gamesSize)
    }

    @Test
    fun `Return the expected value after calling the Repository to get Games when being executed`() {
        val pageNumber = 0
        val gamesSize = 20
        setupRepository(pageNumber, gamesSize)

        val response = getGamesUseCase.execute(pageNumber, gamesSize)

        assertEquals(gamesModel, response)
    }
}
