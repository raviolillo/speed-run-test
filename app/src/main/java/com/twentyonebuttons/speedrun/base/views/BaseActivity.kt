package com.twentyonebuttons.speedrun.base.views

import android.content.pm.ActivityInfo
import android.os.Bundle
import androidx.annotation.VisibleForTesting
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.twentyonebuttons.components.ProgressDialogFragment
import com.twentyonebuttons.presentation.base.contract.BaseContract
import com.twentyonebuttons.speedrun.application.App
import com.twentyonebuttons.speedrun.application.di.components.AppComponent
import com.twentyonebuttons.speedrun.base.behaviours.ProgressDialogBehaviour
import com.twentyonebuttons.speedrun.utils.isBackStackEmpty
import java.lang.reflect.Modifier
import javax.inject.Inject

/**
 * Base activity that must be extended by all activities.
 */
abstract class BaseActivity<in V : BaseContract.View, in R : BaseContract.Router, P : BaseContract.Presenter<V, R>>
    : AppCompatActivity(), BaseContract.View, BaseContract.Router, ProgressDialogBehaviour {

    /**
     * The [BaseContract.Presenter] that will be injected.
     */
    @Inject
    @VisibleForTesting(otherwise = Modifier.PROTECTED)
    lateinit var presenter: P

    /**
     * Specify the layout resource ID to be inflated in the [BaseActivity.onCreate] method.
     */
    abstract val layoutResId: Int

    /**
     * Add the required initializations to inject a custom progress dialog for all activities.
     * It will be used for loading purposes.
     */
    override var progressDialog: DialogFragment? = ProgressDialogFragment()
    override lateinit var progressDialogFragmentManager: FragmentManager
    override var progressDialogLifecycle = lifecycle

    /**
     * Method that overrides the default onBackPressed() to work as desired over any
     * Fragment-Container Activity, which layout contains only a FrameLayout (the container).
     * - If the current fragment shown is the last (back stack empty) it force-finishes itself
     * - Applies the regular behaviour if there are more fragments on the stack when back pressed
     */
    override fun onBackPressed() {
        if (isBackStackEmpty()) {
            finish()
        } else {
            super.onBackPressed()
        }
    }

    /**
     * [AppCompatActivity] lifecycle method onCreate().
     * 1) Inject dependencies (or recover presenter if already have their instance)
     * 2) Get bundle data from any possible option
     * 3) Attach itself as view and call onCreate() to its presenter
     * 4) Init views and setup listeners
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        if (lastCustomNonConfigurationInstance == null) {
            // Init dependencies (if first time)
            injectDependencies((baseContext?.applicationContext as? App)?.component)
        } else {
            // Get presenter from custom instance retained (if not)
            @Suppress("UNCHECKED_CAST")
            presenter = lastCustomNonConfigurationInstance as P
        }

        // Apply regular onCreate method
        super.onCreate(savedInstanceState)

        // Get extras
        retrieveBundleData(savedInstanceState ?: intent.extras ?: Bundle.EMPTY)

        // Avoid rotation in both mobile phones and tablets
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT

        // Set layout
        setContentView(layoutResId)

        // Notify presenter
        @Suppress("UNCHECKED_CAST")
        presenter.attachView(this as V)

        // Call onCreate of the presenter (only if it already existed)
        if (lastCustomNonConfigurationInstance == null) {
            presenter.onCreate()
        }

        // Initialize progress dialog for loading purposes
        progressDialogFragmentManager = supportFragmentManager

        // Init views
        initViews()

        // Setup listeners
        setupListeners()
    }

    /**
     * [AppCompatActivity] lifecycle method onRetainCustomNonConfigurationInstance().
     * Recover presenter.
     */
    override fun onRetainCustomNonConfigurationInstance() = presenter

    /**
     * [AppCompatActivity] lifecycle method onResume().
     * Attach itself as router and call onResume to its presenter.
     */
    override fun onResume() {
        super.onResume()

        // Notify presenter
        @Suppress("UNCHECKED_CAST")
        presenter.attachRouter(this as R)
        presenter.onResume()
    }

    /**
     * [AppCompatActivity] lifecycle method onPause().
     * Detach router and call onPause to its presenter.
     */
    override fun onPause() {
        // Notify presenter
        presenter.detachRouter()
        presenter.onPause()

        super.onPause()
    }

    /**
     * [AppCompatActivity] lifecycle method onDestroy().
     * Detach view and call onDestroy to its presenter.
     */
    override fun onDestroy() {
        // Notify presenter
        presenter.detachView()
        presenter.onDestroy()

        super.onDestroy()
    }

    /**
     * Setup the object graph and inject the dependencies needed on each activity.
     *
     * @param appComponent The required [AppComponent] to inject all the needed dependencies
     */
    abstract fun injectDependencies(appComponent: AppComponent?)

    /**
     * Method to get all the extra data sent via Intent's from another context.
     * It's an optional method since maybe there is not extra data needed.
     *
     * @param bundle The required [Bundle] where the data is compressed
     */
    open fun retrieveBundleData(bundle: Bundle) {
    }

    /**
     * This method is executed on [BaseActivity.onCreate] to init views.
     * It's an optional method, so those extending activities that may need it can override it.
     */
    open fun initViews() {
    }

    /**
     * This method is executed on [BaseActivity.onCreate] to setup view listeners.
     * It's an optional method, so those extending activities that may need it can override it.
     */
    open fun setupListeners() {
    }
}
