package com.twentyonebuttons.speedrun.utils

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.twentyonebuttons.speedrun.R

/**
 * Method to know if back stack is empty, that means, if there is only one [Fragment] in the stack.
 *
 * @return The [Boolean] that indicates if back stack is empty or not
 */
fun FragmentActivity.isBackStackEmpty(): Boolean = supportFragmentManager?.backStackEntryCount == 1

/**
 * Method to navigate into a new fragment of this app.
 *
 * @param fragment The [Fragment] instance we want to open
 * @param contentFrame The Frame Layout container where it will be viewed
 * @param addToBackStack The [Boolean] that indicates if the new fragment should be added to back stack
 */
fun FragmentActivity.navigateToFragment(
    fragment: Fragment,
    contentFrame: Int = R.id.mainContainer,
    addToBackStack: Boolean = true
) {
    supportFragmentManager?.beginTransaction()?.run {
        replace(contentFrame, fragment)
        setBreadCrumbShortTitle(fragment.toString())
        if (addToBackStack) {
            addToBackStack(fragment.toString())
        }
        commit()
    }
}

/**
 * Method used to navigate to the previous fragment.
 */
fun FragmentActivity.navigateBack() {
    supportFragmentManager?.popBackStack()
}

/**
 * Method used to navigate to an external application able to open the retrieved uri.
 *
 * @param uri The [String] format URI to be opened externally
 */
fun Context.navigateToUri(uri: String) {
    startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(uri)))
}
