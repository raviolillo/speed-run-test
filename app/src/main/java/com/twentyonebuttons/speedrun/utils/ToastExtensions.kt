package com.twentyonebuttons.speedrun.utils

import android.content.Context
import android.widget.Toast
import com.twentyonebuttons.speedrun.R

/**
 * Method to show a message or a unknown error by default.
 *
 * @param message the [String] to be shown
 */
fun Context.showMessage(message: String?) {
    longToast(message ?: getString(R.string.unknown_error))
}

/**
 * Method to show a message in a [Toast].
 *
 * @param message The [String] to be shown
 */
fun Context.longToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}
