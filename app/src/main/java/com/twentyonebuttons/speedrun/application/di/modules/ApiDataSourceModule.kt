package com.twentyonebuttons.speedrun.application.di.modules

import com.twentyonebuttons.apidatasource.client.ApiClient
import com.twentyonebuttons.speedrun.BuildConfig
import com.twentyonebuttons.speedrun.application.scopes.PerApplication
import dagger.Module
import dagger.Provides

@Module
class ApiDataSourceModule {

    @Provides
    @PerApplication
    internal fun provideApiClient(): ApiClient = ApiClient(BuildConfig.DEBUG)
}
