package com.twentyonebuttons.speedrun.application

import android.annotation.SuppressLint
import android.app.Application
import com.twentyonebuttons.speedrun.application.di.components.AppComponent
import com.twentyonebuttons.speedrun.application.di.components.DaggerAppComponent
import com.twentyonebuttons.speedrun.application.di.modules.AppModule

/**
 * Base [Application]. Maintains a global application state and uses Dagger to provide dependency
 * injection based on a common [AppComponent] and its subcomponents (classified per scope).
 */
@SuppressLint("Registered")
class App : Application() {

    val component: AppComponent by lazy {
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
    }
}
