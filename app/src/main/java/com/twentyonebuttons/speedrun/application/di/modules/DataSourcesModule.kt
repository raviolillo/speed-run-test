package com.twentyonebuttons.speedrun.application.di.modules

import com.twentyonebuttons.apidatasource.ApiDataSourceImpl
import com.twentyonebuttons.data.datasources.ApiDataSource
import com.twentyonebuttons.speedrun.application.scopes.PerApplication
import dagger.Binds
import dagger.Module

@Module
abstract class DataSourcesModule {

    @Binds
    @PerApplication
    abstract fun providesApiDataSource(apiDataSourceImpl: ApiDataSourceImpl): ApiDataSource
}
