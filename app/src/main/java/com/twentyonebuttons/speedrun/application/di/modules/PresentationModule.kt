package com.twentyonebuttons.speedrun.application.di.modules

import com.twentyonebuttons.presentation.providers.SchedulersProvider
import com.twentyonebuttons.speedrun.application.scopes.PerApplication
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

@Module
class PresentationModule {

    @Provides
    @PerApplication
    internal fun provideSchedulersProvider(): SchedulersProvider = object : SchedulersProvider {

        override fun getScheduler(): Scheduler = AndroidSchedulers.mainThread()
    }
}
