package com.twentyonebuttons.speedrun.application.di.components

import android.content.Context
import com.twentyonebuttons.speedrun.application.App
import com.twentyonebuttons.speedrun.application.di.modules.*
import com.twentyonebuttons.speedrun.application.scopes.PerApplication
import com.twentyonebuttons.speedrun.gamedetails.di.components.GameDetailsComponent
import com.twentyonebuttons.speedrun.gamelist.di.components.GameListComponent
import com.twentyonebuttons.speedrun.main.di.components.MainComponent
import dagger.Component

@PerApplication
@Component(
    modules = [AppModule::class, PresentationModule::class, RepositoriesModule::class, DataSourcesModule::class, ApiDataSourceModule::class]
)
interface AppComponent {

    fun inject(app: App)

    val context: Context

    fun mainComponentBuilder(): MainComponent.Builder

    fun gameListComponentBuilder(): GameListComponent.Builder

    fun gameDetailsComponentBuilder(): GameDetailsComponent.Builder
}
