package com.twentyonebuttons.speedrun.application.di.modules

import com.twentyonebuttons.data.RepositoryImpl
import com.twentyonebuttons.domain.repositories.Repository
import com.twentyonebuttons.speedrun.application.scopes.PerApplication
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoriesModule {

    @Binds
    @PerApplication
    abstract fun providesRepository(repository: RepositoryImpl): Repository
}
