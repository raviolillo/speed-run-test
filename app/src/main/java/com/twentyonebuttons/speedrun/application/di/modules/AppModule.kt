package com.twentyonebuttons.speedrun.application.di.modules

import android.content.Context
import com.twentyonebuttons.speedrun.application.App
import com.twentyonebuttons.speedrun.application.scopes.PerApplication
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val app: App) {

    @Provides
    @PerApplication
    internal fun provideContext(): Context = app
}
