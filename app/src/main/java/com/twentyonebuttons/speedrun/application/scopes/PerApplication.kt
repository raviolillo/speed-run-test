package com.twentyonebuttons.speedrun.application.scopes

import javax.inject.Scope

@Scope
@Retention
annotation class PerApplication
