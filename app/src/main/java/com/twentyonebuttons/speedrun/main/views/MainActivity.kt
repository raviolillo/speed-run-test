package com.twentyonebuttons.speedrun.main.views

import com.twentyonebuttons.domain.models.GameModel
import com.twentyonebuttons.presentation.main.contract.MainContract
import com.twentyonebuttons.speedrun.R
import com.twentyonebuttons.speedrun.application.di.components.AppComponent
import com.twentyonebuttons.speedrun.base.views.BaseActivity
import com.twentyonebuttons.speedrun.gamedetails.views.GameDetailsFragment
import com.twentyonebuttons.speedrun.gamelist.views.GameListFragment
import com.twentyonebuttons.speedrun.utils.navigateToFragment

/**
 * Main Activity of the Application, the default container of all sections.
 */
class MainActivity : BaseActivity<MainContract.View, MainContract.Router, MainContract.Presenter>(),
    MainContract.View, MainContract.Router {

    override val layoutResId = R.layout.ac_main

    override fun injectDependencies(appComponent: AppComponent?) {
        appComponent?.mainComponentBuilder()
            ?.build()
            ?.inject(this)
    }

    override fun navigateToGameListView(onGameSelected: (GameModel) -> Unit) {
        navigateToFragment(GameListFragment.newInstance(this, onGameSelected))
    }

    override fun navigateToGameDetailsView(game: GameModel) {
        navigateToFragment(GameDetailsFragment.newInstance(this, game))
    }
}
