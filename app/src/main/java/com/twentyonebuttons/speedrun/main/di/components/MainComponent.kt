package com.twentyonebuttons.speedrun.main.di.components

import com.twentyonebuttons.speedrun.application.scopes.PerActivity
import com.twentyonebuttons.speedrun.main.di.modules.MainModule
import com.twentyonebuttons.speedrun.main.views.MainActivity
import dagger.Subcomponent

@PerActivity
@Subcomponent(modules = [MainModule::class])
interface MainComponent {

    @Subcomponent.Builder
    interface Builder {

        fun build(): MainComponent
    }

    fun inject(activity: MainActivity)
}
