package com.twentyonebuttons.speedrun.main.di.modules

import com.twentyonebuttons.presentation.main.contract.MainContract
import com.twentyonebuttons.presentation.main.presenter.MainPresenter
import com.twentyonebuttons.speedrun.application.scopes.PerActivity
import dagger.Binds
import dagger.Module

@Module
abstract class MainModule {

    @Binds
    @PerActivity
    internal abstract fun bindsPresenter(mainPresenter: MainPresenter): MainContract.Presenter
}
