package com.twentyonebuttons.speedrun.gamedetails.di.modules

import com.twentyonebuttons.presentation.gamedetails.contract.GameDetailsContract
import com.twentyonebuttons.presentation.gamedetails.presenter.GameDetailsPresenter
import com.twentyonebuttons.speedrun.application.scopes.PerFragment
import dagger.Binds
import dagger.Module

@Module
abstract class GameDetailsModule {

    @Binds
    @PerFragment
    internal abstract fun bindsPresenter(gameDetailsPresenter: GameDetailsPresenter): GameDetailsContract.Presenter
}
