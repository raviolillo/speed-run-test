package com.twentyonebuttons.speedrun.gamedetails.views

import android.content.Context
import com.bumptech.glide.Glide
import com.twentyonebuttons.domain.models.GameModel
import com.twentyonebuttons.presentation.gamedetails.contract.GameDetailsContract
import com.twentyonebuttons.speedrun.R
import com.twentyonebuttons.speedrun.application.di.components.AppComponent
import com.twentyonebuttons.speedrun.base.views.BaseFragment
import com.twentyonebuttons.speedrun.utils.navigateBack
import com.twentyonebuttons.speedrun.utils.navigateToUri
import com.twentyonebuttons.speedrun.utils.newFragmentInstance
import com.twentyonebuttons.speedrun.utils.showMessage
import kotlinx.android.synthetic.main.fr_game_details.*

/**
 * Game Details Fragment that shows a list of speedrun games and allows to navigate to its detail view.
 */
class GameDetailsFragment :
    BaseFragment<GameDetailsContract.View, GameDetailsContract.Router, GameDetailsContract.Presenter>(),
    GameDetailsContract.View, GameDetailsContract.Router {

    override val layoutResId = R.layout.fr_game_details

    private lateinit var game: GameModel

    private fun setGame(game: GameModel) {
        this.game = game
    }

    override fun injectDependencies(appComponent: AppComponent?) {
        appComponent?.gameDetailsComponentBuilder()
            ?.build()
            ?.inject(this)
    }

    override fun initViews() {
        super.initViews()
        activity?.setTitle(R.string.game_details)
        presenter.setup(game)
    }

    override fun setupListeners() {
        super.setupListeners()
        btnBackButton.setOnClickListener { presenter.onBackButtonClicked() }
        btnSeeVideo.setOnClickListener { presenter.onSeeVideoButtonClicked() }
    }

    override fun navigateBack() {
        activity?.navigateBack()
    }

    override fun showProgress() {
        showProgressDialog()
    }

    override fun hideProgress() {
        dismissProgressDialog()
    }

    override fun showError(message: String?) {
        context?.showMessage(message)
    }

    override fun showGameName(name: String) {
        tvGameName.text = name
    }

    override fun showGameLogo(logoUri: String) {
        Glide.with(this)
            .load(logoUri)
            .placeholder(R.drawable.ic_game_placeholder)
            .centerCrop()
            .into(ivGameLogo)
    }

    override fun showSpeedRunPlayerName(name: String?) {
        tvSpeedRunPlayer.text = name ?: getString(R.string.unknown_player)
    }

    override fun showSpeedRunTimeExpended(timeSec: Int) {
        val hours = timeSec / HOUR_TO_SEC
        val minutes = (timeSec % HOUR_TO_SEC) / MIN_TO_SEC
        val seconds = timeSec % MIN_TO_SEC
        tvSpeedRunTime.text = String.format("%02d:%02d:%02d", hours, minutes, seconds)
    }

    override fun enableVideoButton() {
        btnSeeVideo.isEnabled = true
    }

    override fun navigateToVideoView(uri: String) {
        activity?.navigateToUri(uri)
    }

    companion object {
        private const val HOUR_TO_SEC = 3600
        private const val MIN_TO_SEC = 60

        fun newInstance(context: Context?, game: GameModel): GameDetailsFragment =
            newFragmentInstance<GameDetailsFragment>(context).apply {
                setGame(game)
            }
    }
}
