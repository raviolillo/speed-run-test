package com.twentyonebuttons.speedrun.gamedetails.di.components

import com.twentyonebuttons.speedrun.application.scopes.PerFragment
import com.twentyonebuttons.speedrun.gamedetails.di.modules.GameDetailsModule
import com.twentyonebuttons.speedrun.gamedetails.views.GameDetailsFragment
import dagger.Subcomponent

@PerFragment
@Subcomponent(modules = [GameDetailsModule::class])
interface GameDetailsComponent {

    @Subcomponent.Builder
    interface Builder {

        fun build(): GameDetailsComponent
    }

    fun inject(fragment: GameDetailsFragment)
}
