package com.twentyonebuttons.speedrun.gamelist.views

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.twentyonebuttons.domain.models.GameModel
import com.twentyonebuttons.presentation.gamelist.contract.GameListContract
import com.twentyonebuttons.speedrun.R
import com.twentyonebuttons.speedrun.application.di.components.AppComponent
import com.twentyonebuttons.speedrun.base.views.BaseFragment
import com.twentyonebuttons.speedrun.gamelist.listeners.EndlessScrollListener
import com.twentyonebuttons.speedrun.gamelist.views.adapters.GameListAdapter
import com.twentyonebuttons.speedrun.utils.newFragmentInstance
import com.twentyonebuttons.speedrun.utils.showMessage
import kotlinx.android.synthetic.main.fr_game_list.*

/**
 * Game List Fragment that shows a list of games and allows to navigate to its detailed view.
 */
class GameListFragment :
    BaseFragment<GameListContract.View, GameListContract.Router, GameListContract.Presenter>(),
    GameListContract.View, GameListContract.Router {

    private lateinit var onGameSelected: (GameModel) -> Unit

    private lateinit var gameListAdapter: GameListAdapter
    private lateinit var scrollListener: EndlessScrollListener

    override val layoutResId = R.layout.fr_game_list

    private fun setOnGameSelected(onGameSelected: (GameModel) -> Unit) {
        this.onGameSelected = onGameSelected
    }

    override fun injectDependencies(appComponent: AppComponent?) {
        appComponent?.gameListComponentBuilder()
            ?.build()
            ?.inject(this)
    }

    override fun initViews() {
        super.initViews()
        activity?.setTitle(R.string.game_list)
    }

    override fun showProgress() {
        showProgressDialog()
    }

    override fun hideProgress() {
        dismissProgressDialog()
    }

    override fun showGameList(gameList: List<GameModel>, max: Int) {
        gameListAdapter = GameListAdapter(gameList, onGameSelected)
        val linearLayoutManager = LinearLayoutManager(context)
        scrollListener = object : EndlessScrollListener(linearLayoutManager, max) {
            override fun onMaximumScrolled(page: Int, totalItemsCount: Int, view: RecyclerView) {
                presenter.onMaximumScrolled(page)
            }
        }
        with(rvGameList) {
            layoutManager = linearLayoutManager
            adapter = gameListAdapter
            addOnScrollListener(scrollListener)
        }
    }

    override fun updateGameList(gameList: List<GameModel>) {
        with(gameListAdapter) {
            updateGameList(gameList)
            notifyDataSetChanged()
        }
    }

    override fun showError(message: String?) {
        context?.showMessage(message)
    }

    companion object {
        fun newInstance(context: Context?, onGameSelected: (GameModel) -> Unit): GameListFragment =
            newFragmentInstance<GameListFragment>(context).apply {
                setOnGameSelected(onGameSelected)
            }
    }
}
