package com.twentyonebuttons.speedrun.gamelist.views.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.twentyonebuttons.domain.models.GameModel
import com.twentyonebuttons.speedrun.R
import com.twentyonebuttons.speedrun.gamelist.views.holders.GameItemViewHolder

/**
 * [RecyclerView.Adapter] that handles the game selector logic.
 *
 * Uses a [RecyclerView] that contains a list of child views ([GameItemViewHolder]s)
 * for each [GameModel] received.
 */
class GameListAdapter(
    private var gameList: List<GameModel>,
    private val clickLambda: (GameModel) -> Unit
) : RecyclerView.Adapter<GameItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameItemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_game_item, parent, false)
        return GameItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: GameItemViewHolder, position: Int) {
        val item = gameList[position]
        with(holder) {
            setLogo(item.smallLogoUri)
            setName(item.name)
            itemView.setOnClickListener { clickLambda.invoke(gameList[position]) }
        }
    }

    override fun getItemCount(): Int = gameList.size

    fun updateGameList(gameList: List<GameModel>) {
        this.gameList = gameList
    }
}