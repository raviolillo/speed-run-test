package com.twentyonebuttons.speedrun.gamelist.di.components

import com.twentyonebuttons.speedrun.application.scopes.PerFragment
import com.twentyonebuttons.speedrun.gamelist.di.modules.GameListModule
import com.twentyonebuttons.speedrun.gamelist.views.GameListFragment
import dagger.Subcomponent

@PerFragment
@Subcomponent(modules = [GameListModule::class])
interface GameListComponent {

    @Subcomponent.Builder
    interface Builder {

        fun build(): GameListComponent
    }

    fun inject(fragment: GameListFragment)
}
