package com.twentyonebuttons.speedrun.gamelist.di.modules

import com.twentyonebuttons.presentation.gamelist.contract.GameListContract
import com.twentyonebuttons.presentation.gamelist.presenter.GameListPresenter
import com.twentyonebuttons.speedrun.application.scopes.PerFragment
import dagger.Binds
import dagger.Module

@Module
abstract class GameListModule {

    @Binds
    @PerFragment
    internal abstract fun bindsPresenter(gameListPresenter: GameListPresenter): GameListContract.Presenter
}
