package com.twentyonebuttons.speedrun.gamelist.views.holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.twentyonebuttons.speedrun.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_game_item.*

/**
 * [RecyclerView.ViewHolder] that has the layout and its shared properties between all game items.
 */
class GameItemViewHolder(override val containerView: View) :
    RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun setLogo(uri: String) {
        Glide.with(containerView)
            .load(uri)
            .placeholder(R.drawable.ic_game_placeholder)
            .centerCrop()
            .into(ivLogo)
    }

    fun setName(name: String) {
        tvName.text = name
    }
}
