package com.twentyonebuttons.presentation.utils

import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.disposables.Disposable
import io.reactivex.exceptions.OnErrorNotImplementedException
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

private val defaultSchedulerToSubscribeOn: Scheduler = Schedulers.io()
private val onSubscribeStub: () -> Unit = {}
private val onFinallyStub: () -> Unit = {}
private val onErrorStub: (Throwable) -> Unit = { throw OnErrorNotImplementedException(it) }
private val onNextStub: (Any) -> Unit = {}

/**
 * Overloaded subscribe function that allows passing named parameters.
 * Get a disposable from a [Single].
 */
fun <T : Any> Single<T>.rxSubscribe(
    schedulerToObserveOn: Scheduler,
    schedulerToSubscribeOn: Scheduler = defaultSchedulerToSubscribeOn,
    onSubscribe: () -> Unit = onSubscribeStub,
    onFinally: () -> Unit = onFinallyStub,
    onError: (Throwable) -> Unit = onErrorStub,
    onSuccess: (T) -> Unit = onNextStub
): Disposable =
    subscribeOn(schedulerToSubscribeOn)
        .observeOn(schedulerToObserveOn)
        .doOnSubscribe { onSubscribe() }
        .doFinally { onFinally() }
        .subscribeWith(object : DisposableSingleObserver<T>() {
            override fun onError(throwable: Throwable) {
                throwable.printStackTrace()
                onError.invoke(throwable)
            }

            override fun onSuccess(t: T) {
                onSuccess.invoke(t)
            }
        })
