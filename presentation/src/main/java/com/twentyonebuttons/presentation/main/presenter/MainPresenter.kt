package com.twentyonebuttons.presentation.main.presenter

import com.twentyonebuttons.domain.models.GameModel
import com.twentyonebuttons.presentation.base.presenter.BasePresenter
import com.twentyonebuttons.presentation.main.contract.MainContract
import javax.inject.Inject

/**
 * Presenter that manages the data and the logic to the main view.
 */
class MainPresenter @Inject constructor() : BasePresenter<MainContract.View, MainContract.Router>(),
    MainContract.Presenter {

    override fun onCreate() {
        super.onCreate()
        routerAction { navigateToGameListView { onGameSelected(it) } }
    }

    private fun onGameSelected(game: GameModel) {
        routerAction { navigateToGameDetailsView(game) }
    }
}
