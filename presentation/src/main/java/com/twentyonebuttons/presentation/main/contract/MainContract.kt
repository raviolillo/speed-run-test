package com.twentyonebuttons.presentation.main.contract

import com.twentyonebuttons.domain.models.GameModel
import com.twentyonebuttons.presentation.base.contract.BaseContract

interface MainContract {

    interface View : BaseContract.View

    interface Router : BaseContract.Router {

        fun navigateToGameListView(onGameSelected: (GameModel) -> Unit)

        fun navigateToGameDetailsView(game: GameModel)
    }

    interface Presenter : BaseContract.Presenter<View, Router>
}
