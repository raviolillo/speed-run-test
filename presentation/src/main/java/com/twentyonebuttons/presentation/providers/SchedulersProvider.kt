package com.twentyonebuttons.presentation.providers

import io.reactivex.Scheduler

interface SchedulersProvider {

    fun getScheduler(): Scheduler
}
