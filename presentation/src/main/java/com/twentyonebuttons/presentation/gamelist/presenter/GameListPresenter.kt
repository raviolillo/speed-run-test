package com.twentyonebuttons.presentation.gamelist.presenter

import com.twentyonebuttons.domain.models.GameModel
import com.twentyonebuttons.domain.usecases.GetGamesUseCase
import com.twentyonebuttons.presentation.base.presenter.BasePresenter
import com.twentyonebuttons.presentation.gamelist.contract.GameListContract
import com.twentyonebuttons.presentation.providers.SchedulersProvider
import com.twentyonebuttons.presentation.utils.rxSubscribe
import javax.inject.Inject

/**
 * Presenter that manages the data and the logic to obtain a list of games and communicates
 * with its view and router to show them and navigate to the speedrun detailed view accordingly.
 */
class GameListPresenter @Inject constructor(
    private val schedulersProvider: SchedulersProvider,
    private val getGamesUseCase: GetGamesUseCase
) : BasePresenter<GameListContract.View, GameListContract.Router>(), GameListContract.Presenter {

    private var gameList: MutableList<GameModel> = emptyList<GameModel>().toMutableList()

    override fun onCreate() {
        super.onCreate()
        requestGames(INITIAL_GAMES_PAGE)
    }

    private fun requestGames(pageNumber: Int) {
        viewAction { showProgress() }
        getGamesUseCase.execute(pageNumber, DEFAULT_NUMBER_OF_GAMES).rxSubscribe(
            schedulerToObserveOn = schedulersProvider.getScheduler(),
            onSuccess = { games ->
                gameList.addAll(games.data)
                viewAction {
                    hideProgress()
                    if (pageNumber == INITIAL_GAMES_PAGE) {
                        showGameList(gameList, DEFAULT_NUMBER_OF_GAMES)
                    } else {
                        updateGameList(gameList)
                    }
                }
            },
            onError = { throwable ->
                viewAction {
                    hideProgress()
                    showError(throwable.message)
                }
            }
        )
    }

    override fun onMaximumScrolled(pageNumber: Int) {
        requestGames(pageNumber)
    }

    companion object {
        private const val INITIAL_GAMES_PAGE = 0
        private const val DEFAULT_NUMBER_OF_GAMES = 20
    }
}
