package com.twentyonebuttons.presentation.gamelist.contract

import com.twentyonebuttons.domain.models.GameModel
import com.twentyonebuttons.presentation.base.contract.BaseContract

interface GameListContract {

    interface View : BaseContract.View {

        fun showProgress()

        fun hideProgress()

        fun showGameList(gameList: List<GameModel>, max: Int)

        fun updateGameList(gameList: List<GameModel>)

        fun showError(message: String?)
    }

    interface Router : BaseContract.Router

    interface Presenter : BaseContract.Presenter<View, Router> {

        fun onMaximumScrolled(pageNumber: Int)
    }
}
