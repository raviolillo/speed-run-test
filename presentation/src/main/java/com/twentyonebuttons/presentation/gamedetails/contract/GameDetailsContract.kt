package com.twentyonebuttons.presentation.gamedetails.contract

import com.twentyonebuttons.domain.models.GameModel
import com.twentyonebuttons.presentation.base.contract.BaseContract

interface GameDetailsContract {

    interface View : BaseContract.View {

        fun showProgress()

        fun hideProgress()

        fun showError(message: String?)

        fun showGameName(name: String)

        fun showGameLogo(logoUri: String)

        fun showSpeedRunPlayerName(name: String?)

        fun showSpeedRunTimeExpended(timeSec: Int)

        fun enableVideoButton()
    }

    interface Router : BaseContract.Router {

        fun navigateBack()

        fun navigateToVideoView(uri: String)
    }

    interface Presenter : BaseContract.Presenter<View, Router> {

        fun setup(game: GameModel)

        fun onBackButtonClicked()

        fun onSeeVideoButtonClicked()
    }
}
