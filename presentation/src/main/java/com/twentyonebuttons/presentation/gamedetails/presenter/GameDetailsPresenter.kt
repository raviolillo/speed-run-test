package com.twentyonebuttons.presentation.gamedetails.presenter

import com.twentyonebuttons.domain.models.GameModel
import com.twentyonebuttons.domain.models.SpeedRunModel
import com.twentyonebuttons.domain.usecases.GetFastestGameSpeedRunUseCase
import com.twentyonebuttons.presentation.base.presenter.BasePresenter
import com.twentyonebuttons.presentation.gamedetails.contract.GameDetailsContract
import com.twentyonebuttons.presentation.providers.SchedulersProvider
import com.twentyonebuttons.presentation.utils.rxSubscribe
import javax.inject.Inject

/**
 * Presenter that manages the data and the logic to obtain the fastest speedrun of a given game.
 * It communicates with the view to show its information and the first player who accomplished the speedrun.
 */
class GameDetailsPresenter @Inject constructor(
    private val schedulersProvider: SchedulersProvider,
    private val getFastestGameSpeedRunUseCase: GetFastestGameSpeedRunUseCase
) : BasePresenter<GameDetailsContract.View, GameDetailsContract.Router>(),
    GameDetailsContract.Presenter {

    private lateinit var game: GameModel
    private lateinit var speedRun: SpeedRunModel

    override fun setup(game: GameModel) {
        this.game = game
        showKnownGameInfo()
        requestFastestGameSpeedRun()
    }

    private fun showKnownGameInfo() {
        viewAction {
            showGameName(game.name)
            showGameLogo(game.bigLogoUri)
        }
    }

    private fun requestFastestGameSpeedRun() {
        viewAction { showProgress() }
        getFastestGameSpeedRunUseCase.execute(game.id).rxSubscribe(
            schedulerToObserveOn = schedulersProvider.getScheduler(),
            onSuccess = { speedRun ->
                viewAction {
                    hideProgress()
                    showSpeedRunPlayerName(speedRun.firstPlayer.name)
                    showSpeedRunTimeExpended(speedRun.timeSec)
                    if (speedRun.videoUri.isNotEmpty()) {
                        enableVideoButton()
                    }
                }
                this.speedRun = speedRun
            },
            onError = { throwable ->
                viewAction {
                    hideProgress()
                    showError(throwable.message)
                }
            }
        )
    }

    override fun onBackButtonClicked() {
        routerAction { navigateBack() }
    }

    override fun onSeeVideoButtonClicked() {
        routerAction { navigateToVideoView(speedRun.videoUri) }
    }
}
