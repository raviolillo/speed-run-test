package com.twentyonebuttons.presentation.utils

import org.mockito.Mockito

fun <T> any(): T = Mockito.any<T>()
