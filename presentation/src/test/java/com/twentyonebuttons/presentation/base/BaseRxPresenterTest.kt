package com.twentyonebuttons.presentation.base

import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.mockito.MockitoAnnotations

/**
 * Base Presenter Tests that must be extended by all Presenter Tests that use [RxJavaPlugins]
 */
open class BaseRxPresenterTest {

    @Before
    open fun setup() {
        MockitoAnnotations.initMocks(this)
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setComputationSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setNewThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setSingleSchedulerHandler { Schedulers.trampoline() }
    }

    @After
    @Throws(Exception::class)
    fun tearDown() = RxJavaPlugins.reset()
}
