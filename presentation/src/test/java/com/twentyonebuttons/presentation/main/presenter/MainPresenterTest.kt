package com.twentyonebuttons.presentation.main.presenter

import com.twentyonebuttons.presentation.main.contract.MainContract
import com.twentyonebuttons.presentation.utils.any
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.only
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

class MainPresenterTest {

    @Mock
    private lateinit var mainRouter: MainContract.Router

    private lateinit var mainPresenter: MainPresenter

    /**
     * Setup Mockito here
     */

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        mainPresenter = MainPresenter()
    }

    /**
     * Test presenter
     */

    @Test
    fun `Call the Router to navigate to the game list view when created`() {
        mainPresenter.attachRouter(mainRouter)
        mainPresenter.onCreate()

        verify(mainRouter, only()).navigateToGameListView(any())
    }
}
