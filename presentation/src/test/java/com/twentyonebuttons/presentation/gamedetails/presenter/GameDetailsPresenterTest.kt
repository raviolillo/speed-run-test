package com.twentyonebuttons.presentation.gamedetails.presenter

import com.twentyonebuttons.domain.models.GameModel
import com.twentyonebuttons.domain.models.SpeedRunModel
import com.twentyonebuttons.domain.models.SpeedRunPlayerModel
import com.twentyonebuttons.domain.usecases.GetFastestGameSpeedRunUseCase
import com.twentyonebuttons.presentation.base.BaseRxPresenterTest
import com.twentyonebuttons.presentation.gamedetails.contract.GameDetailsContract
import com.twentyonebuttons.presentation.providers.SchedulersProvider
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*

class GameDetailsPresenterTest : BaseRxPresenterTest() {

    @Mock
    private lateinit var schedulersProvider: SchedulersProvider
    @Mock
    private lateinit var getFastestGameSpeedRunUseCase: GetFastestGameSpeedRunUseCase
    @Mock
    private lateinit var gameModel: GameModel
    @Mock
    private lateinit var speedRunModel: SpeedRunModel
    @Mock
    private lateinit var speedRunPlayerModel: SpeedRunPlayerModel
    @Mock
    private lateinit var gameDetailsView: GameDetailsContract.View
    @Mock
    private lateinit var gameDetailsRouter: GameDetailsContract.Router

    private lateinit var gameDetailsPresenter: GameDetailsPresenter

    /**
     * Setup Mockito here
     */

    @Before
    override fun setup() {
        super.setup()
        `when`(schedulersProvider.getScheduler())
            .thenReturn(Schedulers.trampoline())
        gameDetailsPresenter =
            GameDetailsPresenter(schedulersProvider, getFastestGameSpeedRunUseCase)
    }

    /**
     * Test presenter
     */

    @Test
    fun `Call view to show correct game info when setup invoked`() {
        `when`(getFastestGameSpeedRunUseCase.execute(gameModel.id))
            .thenReturn(Single.just(speedRunModel))
        `when`(speedRunModel.firstPlayer)
            .thenReturn(speedRunPlayerModel)
        `when`(speedRunModel.videoUri)
            .thenReturn("")

        gameDetailsPresenter.attachView(gameDetailsView)
        gameDetailsPresenter.setup(gameModel)

        verify(gameDetailsView).showGameName(gameModel.name)
        verify(gameDetailsView).showGameName(gameModel.bigLogoUri)
    }

    @Test
    fun `Call view to show correct fastest speed run time and player name if use case executed successfully when setup invoked`() {
        `when`(getFastestGameSpeedRunUseCase.execute(gameModel.id))
            .thenReturn(Single.just(speedRunModel))
        `when`(speedRunModel.firstPlayer)
            .thenReturn(speedRunPlayerModel)
        `when`(speedRunModel.videoUri)
            .thenReturn("")

        gameDetailsPresenter.attachView(gameDetailsView)
        gameDetailsPresenter.setup(gameModel)

        verify(gameDetailsView).showSpeedRunPlayerName(speedRunModel.firstPlayer.name)
        verify(gameDetailsView).showSpeedRunTimeExpended(speedRunModel.timeSec)
    }

    @Test
    fun `Call view to enable video button if use case executed successfully and video returned when setup invoked`() {
        val videoReturned = "test_video_uri"
        `when`(getFastestGameSpeedRunUseCase.execute(gameModel.id))
            .thenReturn(Single.just(speedRunModel))
        `when`(speedRunModel.firstPlayer)
            .thenReturn(speedRunPlayerModel)
        `when`(speedRunModel.videoUri)
            .thenReturn(videoReturned)

        gameDetailsPresenter.attachView(gameDetailsView)
        gameDetailsPresenter.setup(gameModel)

        verify(gameDetailsView).enableVideoButton()
    }

    @Test
    fun `Do not call view to enable video button if use case executed successfully but no video returned when setup invoked`() {
        val noVideoReturned = ""
        `when`(getFastestGameSpeedRunUseCase.execute(gameModel.id))
            .thenReturn(Single.just(speedRunModel))
        `when`(speedRunModel.firstPlayer)
            .thenReturn(speedRunPlayerModel)
        `when`(speedRunModel.videoUri)
            .thenReturn(noVideoReturned)

        gameDetailsPresenter.attachView(gameDetailsView)
        gameDetailsPresenter.setup(gameModel)

        verify(gameDetailsView, never()).enableVideoButton()
    }

    @Test
    fun `Call view to show error if use case executed unsuccessfully when setup invoked`() {
        val error = "test_error"
        `when`(getFastestGameSpeedRunUseCase.execute(gameModel.id))
            .thenReturn(Single.error(Throwable(error)))

        gameDetailsPresenter.attachView(gameDetailsView)
        gameDetailsPresenter.setup(gameModel)

        verify(gameDetailsView).showError(error)
    }

    @Test
    fun `Call router to navigate back if back button clicked invoked`() {
        gameDetailsPresenter.attachRouter(gameDetailsRouter)
        gameDetailsPresenter.onBackButtonClicked()

        verify(gameDetailsRouter, only()).navigateBack()
    }

    @Test
    fun `Call router to navigate to video view when see video button clicked`() {
        `when`(getFastestGameSpeedRunUseCase.execute(gameModel.id))
            .thenReturn(Single.just(speedRunModel))

        gameDetailsPresenter.attachRouter(gameDetailsRouter)
        gameDetailsPresenter.setup(gameModel)
        gameDetailsPresenter.onSeeVideoButtonClicked()

        verify(gameDetailsRouter, only()).navigateToVideoView(speedRunModel.videoUri)
    }
}
