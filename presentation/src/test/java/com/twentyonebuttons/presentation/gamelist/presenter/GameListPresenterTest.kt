package com.twentyonebuttons.presentation.gamelist.presenter

import com.twentyonebuttons.domain.models.GamesModel
import com.twentyonebuttons.domain.usecases.GetGamesUseCase
import com.twentyonebuttons.presentation.base.BaseRxPresenterTest
import com.twentyonebuttons.presentation.gamelist.contract.GameListContract
import com.twentyonebuttons.presentation.providers.SchedulersProvider
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*

class GameListPresenterTest : BaseRxPresenterTest() {

    @Mock
    private lateinit var schedulersProvider: SchedulersProvider
    @Mock
    private lateinit var getGamesUseCase: GetGamesUseCase
    @Mock
    private lateinit var gamesModel: GamesModel
    @Mock
    private lateinit var gameListView: GameListContract.View

    private lateinit var gameListPresenter: GameListPresenter

    /**
     * Setup Mockito here
     */

    @Before
    override fun setup() {
        super.setup()
        `when`(schedulersProvider.getScheduler())
            .thenReturn(Schedulers.trampoline())
        gameListPresenter = GameListPresenter(schedulersProvider, getGamesUseCase)
    }

    /**
     * Test presenter
     */

    @Test
    fun `Execute Game Use Case to get initial games when created`() {
        val initialPage = 0
        `when`(getGamesUseCase.execute(initialPage))
            .thenReturn(Single.just(gamesModel))

        gameListPresenter.onCreate()

        verify(getGamesUseCase, only()).execute(initialPage)
    }

    @Test
    fun `Call view to show error message when execute game use case provokes error getting games`() {
        val initialPage = 0
        val errorMessage = "test_error"
        `when`(getGamesUseCase.execute(initialPage))
            .thenReturn(Single.error(Throwable(errorMessage)))

        gameListPresenter.attachView(gameListView)
        gameListPresenter.onCreate()

        verify(gameListView).showError(errorMessage)
    }

    @Test
    fun `Call view to show game list when execute game use case gets successfully the initial games`() {
        val initialPage = 0
        val gamesSize = 20
        `when`(getGamesUseCase.execute(initialPage))
            .thenReturn(Single.just(gamesModel))

        gameListPresenter.attachView(gameListView)
        gameListPresenter.onCreate()

        verify(gameListView).showGameList(gamesModel.data, gamesSize)
    }

    @Test
    fun `Call view to update game list when execute game use case gets successfully new games`() {
        val newPage = 2
        `when`(getGamesUseCase.execute(newPage))
            .thenReturn(Single.just(gamesModel))

        gameListPresenter.attachView(gameListView)
        gameListPresenter.onMaximumScrolled(newPage)

        verify(gameListView).updateGameList(gamesModel.data)
    }
}
