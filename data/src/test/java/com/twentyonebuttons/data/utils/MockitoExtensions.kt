package com.twentyonebuttons.data.utils

import org.mockito.Mockito

fun <T> any(): T = Mockito.any<T>()
