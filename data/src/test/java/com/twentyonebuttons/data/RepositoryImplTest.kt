package com.twentyonebuttons.data

import com.twentyonebuttons.data.datasources.ApiDataSource
import com.twentyonebuttons.data.utils.any
import com.twentyonebuttons.domain.models.GamesModel
import com.twentyonebuttons.domain.models.PlayerDetailsModel
import com.twentyonebuttons.domain.models.SpeedRunsModel
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations

class RepositoryImplTest {

    @Mock
    private lateinit var apiDataSource: ApiDataSource
    @Mock
    private lateinit var gamesModel: GamesModel
    @Mock
    private lateinit var speedRunsModel: SpeedRunsModel
    @Mock
    private lateinit var playerDetailsModel: PlayerDetailsModel

    private lateinit var repositoryImpl: RepositoryImpl

    /**
     * Setup Mockito here
     */

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        repositoryImpl = RepositoryImpl(apiDataSource)
    }

    /**
     * Test getGames() method
     */

    private fun mockApiDataSourceGetGames(pageNumber: Int, gamesSize: Int) {
        `when`(apiDataSource.getGames(pageNumber, gamesSize))
            .thenReturn(Single.just(gamesModel))
    }

    @Test
    fun `Call Api Data Source to get Games when getGames() invoked`() {
        val pageNumber = 0
        val gamesSize = 20
        mockApiDataSourceGetGames(pageNumber, gamesSize)

        repositoryImpl.getGames(pageNumber, gamesSize)

        verify(apiDataSource, only()).getGames(pageNumber, gamesSize)
    }

    @Test
    fun `Return the expected value after calling Api Data Source to get Games when getGames() invoked`() {
        val pageNumber = 0
        val gamesSize = 20
        mockApiDataSourceGetGames(pageNumber, gamesSize)

        repositoryImpl.getGames(pageNumber, gamesSize)

        assertEquals(gamesModel, apiDataSource.getGames(pageNumber, gamesSize).blockingGet())
    }

    /**
     * Test getGameSpeedRuns() method
     */

    private fun mockApiDataSourceGetGameSpeedRuns() {
        `when`(apiDataSource.getGameSpeedRuns(any()))
            .thenReturn(Single.just(speedRunsModel))
    }

    @Test
    fun `Call Api Data Source to get Speed Runs when getGameSpeedRuns() invoked`() {
        val gameId = "test1"
        mockApiDataSourceGetGameSpeedRuns()

        repositoryImpl.getFastestGameSpeedRun(gameId)

        verify(apiDataSource, only()).getGameSpeedRuns(gameId)
    }

    @Test
    fun `Return the expected value after calling Api Data Source to get Speed Runs when getGameSpeedRuns() invoked`() {
        val gameId = "test2"
        mockApiDataSourceGetGameSpeedRuns()

        repositoryImpl.getFastestGameSpeedRun(gameId)

        assertEquals(speedRunsModel, apiDataSource.getGameSpeedRuns(gameId).blockingGet())
    }

    /**
     * Test getPlayerDetails() method
     */

    private fun mockApiDataSourceGetPlayerDetails() {
        `when`(apiDataSource.getPlayerDetails(any()))
            .thenReturn(Single.just(playerDetailsModel))
    }

    @Test
    fun `Call Api Data Source to get player info when getPlayerDetails() invoked`() {
        val userId = "test1"
        mockApiDataSourceGetPlayerDetails()

        repositoryImpl.getPlayerDetails(userId)

        verify(apiDataSource, only()).getPlayerDetails(userId)
    }

    @Test
    fun `Return the expected value after calling Api Data Source to get player info when getPlayerDetails() invoked`() {
        val userId = "test2"
        mockApiDataSourceGetPlayerDetails()

        repositoryImpl.getPlayerDetails(userId)

        assertEquals(playerDetailsModel, apiDataSource.getPlayerDetails(userId).blockingGet())
    }
}
