package com.twentyonebuttons.data

import com.twentyonebuttons.data.datasources.ApiDataSource
import com.twentyonebuttons.domain.models.GamesModel
import com.twentyonebuttons.domain.models.PlayerDetailsModel
import com.twentyonebuttons.domain.models.SpeedRunModel
import com.twentyonebuttons.domain.repositories.Repository
import io.reactivex.Single
import javax.inject.Inject

class RepositoryImpl @Inject constructor(
    private val apiDataSource: ApiDataSource
) : Repository {

    override fun getGames(pageNumber: Int, max: Int): Single<GamesModel> =
        apiDataSource.getGames(pageNumber, max)

    override fun getFastestGameSpeedRun(gameId: String): Single<SpeedRunModel> {
        var fastestSpeedRun = SpeedRunModel()
        return apiDataSource.getGameSpeedRuns(gameId)
            .map {
                fastestSpeedRun = it.data
                    .sortedBy { speedRunModel -> speedRunModel.timeSec }
                    .first { speedRunModel -> speedRunModel.timeSec > 0 }
                fastestSpeedRun
            }
            .filter { it.firstPlayer.name.isNullOrEmpty() && it.firstPlayer.id?.isNotEmpty() == true }
            .flatMapSingle { speedRun ->
                getPlayerDetails(speedRun.firstPlayer.id ?: "").map { playerDetails ->
                    speedRun.firstPlayer.name = playerDetails.data.name
                    speedRun
                }.onErrorReturn { fastestSpeedRun }
            }.onErrorReturn { fastestSpeedRun }
    }

    override fun getPlayerDetails(userId: String): Single<PlayerDetailsModel> =
        apiDataSource.getPlayerDetails(userId)
}
