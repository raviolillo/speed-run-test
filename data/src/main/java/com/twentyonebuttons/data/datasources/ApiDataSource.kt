package com.twentyonebuttons.data.datasources

import com.twentyonebuttons.domain.models.GamesModel
import com.twentyonebuttons.domain.models.PlayerDetailsModel
import com.twentyonebuttons.domain.models.SpeedRunsModel
import io.reactivex.Single

interface ApiDataSource {

    fun getGames(pageNumber: Int, max: Int): Single<GamesModel>

    fun getGameSpeedRuns(gameId: String): Single<SpeedRunsModel>

    fun getPlayerDetails(userId: String): Single<PlayerDetailsModel>
}
